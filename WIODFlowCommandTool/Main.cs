using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using System.Reflection;
using System.IO;

using DFlow;
using DFlowCore;

using WIODatasetLanguage;
using WIODataLanguage;

using MongoDB.Bson;

namespace WIO.Util
{

		//Usage: dflow -d <dataset_parse_expr> -m <file with model bson> -o <output path>
	//Besides it will look for DFLOW_ASSEMBLY_PATH and DFLOW_DATASET_PATH in env, for loading
	//Assemblies and dataset
	//TODO: plugins
	public class CommandTool{
		public Dictionary<string, string> parameters_dict  = new Dictionary<string,string>();
		public Dictionary<string, string> arguments = new Dictionary<string,string>();
		public Dictionary<string, string> help_dict = new Dictionary<string, string>(); //todo: fill up
		
		
		public CommandTool(){
			parameters_dict["-d"] = "dataset_expr";
			parameters_dict["-m"] = "model_expr_source";
			parameters_dict["-t"] = "training_dataset";
			parameters_dict["-h"] = "help";
			parameters_dict["-h"] = "cache_dataset";
	
			help_dict["-d"] = "Dataset expression : WIODatasetLanguage expression for the main" +
				"dataset we are interested in ";
			help_dict["-m"] = "Model expression source : path to file with json representation of" +
				"dflow model";
			help_dict["-t"] = "Training dataset : if the dataflow is in RequiredTraining state this parameters" +
				"HAS TO BE set";
			help_dict["-c"] = "Cache dataset : you might specify dataset language expression for dataset" +
				"you wish to cache and will be used in training or sorce dataset";
		}

		public string DumpHelp(){
			string tmp = "";
			foreach(var x in help_dict.Keys)
				tmp+= x.ToString() +":" + help_dict[x].ToString() + "\n";
			return tmp;
		}
		
		public void Load(string[] args){
			for(int i=0;i<args.Length;++i)
				if(parameters_dict.ContainsKey(args[i]))
					if(i!=args.Length -1 ) arguments[parameters_dict[args[i]]] = args[i+1];	
			else arguments[parameters_dict[args[i]]] = "";
		
			//help requested
			if(arguments.ContainsKey("help")){
				Console.WriteLine(this.DumpHelp());
				return;
			}
			
			
			try{
			Console.WriteLine("DFLOWCMDTOOL:{0}:{1}", 
			                  arguments["dataset_expr"], 
			                  arguments["model_expr_source"]);
			}
			catch(Exception e){
				throw new Exception("Probably wrong parameters supplied, see --h for more information");	
			}
			
			Console.WriteLine("DFLOWCMDTOOL:DFLOW_DATASET_PATH={0}",Environment.GetEnvironmentVariable("DFLOW_DATASET_PATH"));
	
			foreach(string path in Environment.GetEnvironmentVariable("DFLOW_DATASET_PATH").Split (':')){
				if(path=="") continue;
				Console.WriteLine("DFLOWCMDTOOL:Loading {0} datasets",path);
				WIODatasetLanguageEnv.Instance.RecursivelyLoadAssemblies(path);
			}
	
			
			Console.WriteLine("DFLOWCMDTOOL:Loaded assemblies");
			
			
			foreach(string path in Environment.GetEnvironmentVariable("DFLOW_PATH").Split (':')){
				if(path=="") continue;
				Console.WriteLine("DFLOWCMDTOOL: scanning for assemblies "+path);
				_RecursivelyLoadAssemblies(path);
			}
				
			//Look for new assemblies loaded
			DFlowCore.DFlowEngineGlobalRegister.DoLookForNewAssemblies();
			
			Console.WriteLine("DFLOWCMDTOOL:Loaded plugins");
	
					if(arguments.ContainsKey("cache_dataset")){
				Console.WriteLine("DFLOWCMDTOOL:Loading the cache dataset");
				WIODatasetLanguageEnv.Instance.EvaluateDirectly(
				this.arguments["cache_dataset"]);
				Console.WriteLine("DFLOWCMDTOOL:Loaded the cache dataset");
			}						
			
			Console.WriteLine("DFLOWCMDTOOL:Loading the datasets");
			IDataset training_dataset = null;
			if(arguments.ContainsKey("training_dataset")){
				Console.WriteLine("DFLOWCMDTOOL:Loading the training dataset");
				training_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(
				this.arguments["training_dataset"]);
				Console.WriteLine("DFLOWCMDTOOL:Loaded the training dataset");
			}			
	
			
			
			IDataset source_dataset = (IDataset)WIODatasetLanguageEnv.Instance.EvaluateDirectly(
				this.arguments["dataset_expr"]);
			Console.WriteLine("DFLOWCMDTOOL:Loaded the datasets");
			
			

			
			
			string bson_command = (string) this.arguments["model_expr_source"];// File.ReadAllLines(this.arguments["model_expr_source"])[0];
			
			Console.WriteLine("DFLOWCMDTOOL: Parsing "+bson_command+" to dflow model");
			
			
			Dataflow master_dataflow = new Dataflow();
			master_dataflow.Load (
				MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(bson_command));

			
//			//Omitted for now : LoadModel needs type and it is a little cumbersome
//			//Here only for testing 
//			//Not correct *LoadModel(object)*!, I think LoadModel should itself find type of
//			//persistent data
//			//Another problem is that BsonDocument doesn't allow for persistent data > 4mb, which
//			//doesnt sound good
//			if(this.arguments.ContainsKey("try_load")){
//				Console.WriteLine("DFLOWCMDTOOL: Loading persistent data");
//				int cnt = 0;
//				foreach(var x in master_dataflow.GetNodes()){
//					if(x is PersistentNode) ((PersistentNode)x).LoadModel(typeof(object));
//					++cnt;
//				}
//			}
			
		
			
			List<List<object>> output = new List<List<object>>();
			
			
			if(master_dataflow.State == Dataflow.DataflowState.RequiresTraining){
				//train it
				if(training_dataset == null) throw new Exception("Not provided training dataset");
				master_dataflow.source_dataset = training_dataset;
				
				while(master_dataflow.State == Dataflow.DataflowState.RequiresTraining){
					foreach(var key in training_dataset.Keys()){

						master_dataflow.data_address = key;
						master_dataflow.Process();
						
					}
				}
			}
			
			if(master_dataflow.State != Dataflow.DataflowState.Ready){
				throw new Exception("Not ready dataflow. Error in training , or not corrected" +
				                    "model provided");
			}
			
			
			
			DFlow.Node LastNode = null;
			foreach(var node in master_dataflow.GetNodes()) LastNode = node;
			
			//Note : there is a problem with trainability : if it is a trainable node
			//First item just won't fire.. I can check it here or change trainable
			Console.WriteLine("DFLOWCMDTOOL:Master dataflow created");
			Console.WriteLine("DFLOWCMDTOOL:Running dataflow on the provided dataset..");
			master_dataflow.source_dataset = source_dataset;	              
			foreach(var k in source_dataset.Keys()){
				master_dataflow.data_address = k;
				master_dataflow.Process();
			}
			Console.WriteLine("DFLOWCMDTOOL:Running dataflow success");
			
			
			
			
			
			Console.WriteLine("DFLOWCMDTOOL:Finished");
		}
		

		private void _RecursivelyLoadAssemblies(string path){
			if(path.EndsWith(".dll")){
				Console.WriteLine("DFLOWCMDTOOL:Loaded "+path);
				
				DFlowEngineGlobalRegister.RegisterAssembly(Assembly.LoadFile(path));
				return;
			}
			
			
			foreach(string file in Directory.GetFiles (path)){
				if(file.EndsWith(".dll")){
					Console.WriteLine("DFLOWCMDTOOL:Loaded "+file);
						DFlowEngineGlobalRegister.RegisterAssembly(Assembly.LoadFile(file));
				}
			}			
			foreach(string directory in Directory.GetDirectories(path))
				_RecursivelyLoadAssemblies(directory);
			
		}		
		
	}
	public static class MainClass{
		public static void Main(string[] args){
			(new CommandTool()).Load (args);
				
		}
	}


}
